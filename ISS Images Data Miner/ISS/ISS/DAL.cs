﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISS
{
    public static class DAL
    {
        private static string ConnectionString = "<your mysql connection string>";

        //deletes everything from the initial table
        public static void ClearFTPFileEntries()
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("DELETE FROM FTPFileEntries", conn))
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //add an entry to the initial bucket table
        public static void InsertFTPFileEntry(FTPFileEntry entry)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("INSERT INTO FTPFileEntries (Address, CountryDescription, FTPDirectory, FTPFileDateTime, FTPFileName, FullFTPPath, Latitude, Longitude, NumericLatitude, NumericLongitude) VALUES (@paramAddress, @paramCountryDescription, @paramFTPDirectory, @paramFTPFileDateTime, @paramFTPFileName, @paramFullFTPPath, @paramLatitude, @paramLongitude, @paramNumericLatitude, @paramNumericLongitude)", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@paramAddress", entry.Address);
                cmd.Parameters.AddWithValue("@paramCountryDescription", entry.CountryDescription);
                cmd.Parameters.AddWithValue("@paramFTPDirectory", entry.FTPDirectory);
                cmd.Parameters.AddWithValue("@paramFTPFileDateTime", entry.FTPFileDateTime);
                cmd.Parameters.AddWithValue("@paramFTPFileName", entry.FTPFileName);
                cmd.Parameters.AddWithValue("@paramFullFTPPath", entry.FullFTPPath);
                cmd.Parameters.AddWithValue("@paramLatitude", entry.Latitude);
                cmd.Parameters.AddWithValue("@paramLongitude", entry.Longitude);
                cmd.Parameters.AddWithValue("@paramNumericLatitude", entry.NumericLatitude);
                cmd.Parameters.AddWithValue("@paramNumericLongitude", entry.NumericLongitude);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //read everything from the bucket
        public static List<FTPFileEntry> GetFTPFileEntries()
        {
            List<FTPFileEntry> retlist = new List<FTPFileEntry>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT FTPFileEntryId, Address, CountryDescription, FTPDirectory, FTPFileDateTime, FTPFileName, FullFTPPath, Latitude, Longitude, NumericLatitude, NumericLongitude, OpenMapsOrgData FROM FTPFileEntries", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                conn.Open();

                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new FTPFileEntry(dr));
                }

                conn.Close();
            }
            return retlist;
        }

        //get the new, lean entities
        public static List<ISSImage> GetISSImages()
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT FTPFileEntryId, Address, CountryDescription, FTPDirectory, FTPFileDateTime, FTPFileName, FullFTPPath, Latitude, Longitude, NumericLatitude, NumericLongitude, OpenMapsOrgData FROM FTPFileEntries", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                conn.Open();

                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr, true));
                }

                conn.Close();
            }
            return retlist;
        }

        //delete all image entities
        public static void ClearISSImages()
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("DELETE FROM ISSImages", conn))
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //insert a new ISS Image entity
        public static void InsertISSImage(ISSImage image)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("INSERT INTO ISSImages (GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode) VALUES (@paramGoogleAddress, @paramGoogleCountry, @paramFTPDirectory, @paramImageDateTime, @paramFileName, @paramLexicalLatitude, @paramLexicalLongitude, @paramNumericLatitude, @paramNumericLongitude, @paramOpenMapsRoad, @paramOpenMapsCity, @paramOpenMapsStateDistrict, @paramOpenMapsState, @paramOpenMapsCountry, @paramOpenMapsCountryCode)", conn))
            {
                conn.Open();

                cmd.Parameters.AddWithValue("@paramGoogleAddress", image.GoogleAddress);
                cmd.Parameters.AddWithValue("@paramGoogleCountry", image.GoogleCountry);
                cmd.Parameters.AddWithValue("@paramFTPDirectory", image.FTPDirectory);
                cmd.Parameters.AddWithValue("@paramImageDateTime", image.ImageDateTime);
                cmd.Parameters.AddWithValue("@paramFileName", image.FileName);
                cmd.Parameters.AddWithValue("@paramLexicalLatitude", image.LexicalLatitude);
                cmd.Parameters.AddWithValue("@paramLexicalLongitude", image.LexicalLongitude);
                cmd.Parameters.AddWithValue("@paramNumericLatitude", image.NumericLatitude);
                cmd.Parameters.AddWithValue("@paramNumericLongitude", image.NumericLongitude);
                cmd.Parameters.AddWithValue("@paramOpenMapsRoad", image.OpenMapsRoad);
                cmd.Parameters.AddWithValue("@paramOpenMapsCity", image.OpenMapsCity);
                cmd.Parameters.AddWithValue("@paramOpenMapsStateDistrict", image.OpenMapsStateDistrict);
                cmd.Parameters.AddWithValue("@paramOpenMapsState", image.OpenMapsState);
                cmd.Parameters.AddWithValue("@paramOpenMapsCountry", image.OpenMapsCountry);
                cmd.Parameters.AddWithValue("@paramOpenMapsCountryCode", image.OpenMapsCountryCode);
                
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //update an initial bucket entry
        public static void UpdateFTPFileEntryOpenMapOrgData(FTPFileEntry entry)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("UPDATE FTPFileEntries SET OpenMapsOrgData=@paramOpenMapsOrgData WHERE FTPFileEntryId=@paramFTPFileEntryId", conn))
            {
                conn.Open();

                cmd.Parameters.AddWithValue("@paramOpenMapsOrgData", entry.GeoCodingXMLOpenMapsOrg);
                cmd.Parameters.AddWithValue("@paramFTPFileEntryId", entry.FTPFileEntryId);
                cmd.ExecuteNonQuery();
                conn.Close();
            }        
        }


    }
}
