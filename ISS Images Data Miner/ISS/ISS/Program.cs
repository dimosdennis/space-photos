﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ISS
{
    class Program
    {
        private static void Init()
        {
            //hardcoded ISERV FTP Directories
            FTPDirs = new List<string>();
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/03");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/04");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/05");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/06");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/07");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/08");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/09");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/10");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/11");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2013/12");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2014/01");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2014/02");
            FTPDirs.Add("ftp://ghrc.nsstc.nasa.gov/pub/iserv/data/L0/2014/03");

            FTPFileEntries = new List<FTPFileEntry>();
            ISSImages = new List<ISSImage>();
        }

        public static List<FTPFileEntry> FTPFileEntries { get; set; }

        public static List<ISSImage> ISSImages { get; set; }

        public static List<string> FTPDirs { get; set; }

        //this is the main console menu
        static void Main(string[] args)
        {
            Init();
            bool menuOn = true;
            while (menuOn)
            {
                DisplayMenu();

                var key = Console.ReadKey(true).Key;

                switch (key)
                {
                    case ConsoleKey.Escape:
                        menuOn = false;
                        break;
                    case ConsoleKey.F1:
                        Selected_F1();
                        break;
                    case ConsoleKey.F2:
                        Selected_F2();
                        break;
                    case ConsoleKey.F3:
                        Selected_F3();
                        break;
                    case ConsoleKey.F4:
                        Selected_F4();
                        break;
                    case ConsoleKey.F5:
                        Selected_F5();
                        break;
                    case ConsoleKey.F6:
                        Selected_F6();
                        break;
                    case ConsoleKey.F7:
                        Selected_F7();
                        break;
                    case ConsoleKey.F8:
                        Selected_F8();
                        break;
                    case ConsoleKey.F9:
                        break;
                    case ConsoleKey.F10:
                        break;
                    case ConsoleKey.F11:
                        break;
                    case ConsoleKey.F12:

                        break;
                    default:
                        break;
                }
            }
        }

        public static void DisplayMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("****************************************");
            Console.ForegroundColor = FTPFileEntries.Count != 0 ? ConsoleColor.Cyan : ConsoleColor.Red;
            Console.WriteLine("Memory Contents (FTPFileEntries): {0} Entries", FTPFileEntries.Count); 
            Console.ForegroundColor = ISSImages.Count != 0 ? ConsoleColor.Cyan : ConsoleColor.Red;
            Console.WriteLine("Memory Contents (ISSImages): {0} Entries", ISSImages.Count);
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Make your selection:");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("F1 : Download Image information and GeoCode");
            Console.WriteLine("F2 : Read Existing XML File");
            Console.WriteLine("F3 : Clear existing DB and Insert data (FTPFileEntries)");
            Console.WriteLine("F4 : Set OpenMapsOrg Data");
            Console.WriteLine("F5 : Read Data from DB (FTPFileEntries)");
            Console.WriteLine("F6 : Read Data from DB (ISSImages)");
            Console.WriteLine("F7 : Clear existing DB and Insert data (ISSImages)");
            Console.WriteLine("F8 : Create Images");
            Console.WriteLine("ESC: Exit");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("****************************************");
        }

        private static void ReturnToMenu()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Press <enter> to return to the main menu.");
            Console.ReadLine();
        }

        #region F1

        private static void Selected_F1()
        {
            bool f1_continue = true;
            while (f1_continue)
            {
                Console.Clear();
                Console.WriteLine("Are you sure? This will take some time. (Y/N)");
                var selection = Console.ReadKey().Key;
                if (selection == ConsoleKey.Y)
                {
                    //do work
                    //keep this commented out, just to avoid doing the whole thing all over again
                    //GetFileEntriesAndGeoCodingData();
                    f1_continue = false;
                }
                else if (selection == ConsoleKey.N)
                {
                    f1_continue = false;
                }
                else
                {
                    f1_continue = true;
                }
            }
        }

        public static void GetFileEntriesAndGeoCodingData()
        {
            foreach (var dir in FTPDirs)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("***********************************************");
                Console.WriteLine("Getting contents from {0}", dir);
                Console.WriteLine("***********************************************");

                ReadAllFilesFromDir(dir);

            }

            //add geocoding data

            int counter = 1;
            foreach (var item in FTPFileEntries)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("{0} / {1}", counter, FTPFileEntries.Count);
                SetGoogleData(item);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Waiting...");
                Thread.Sleep(1000);
                counter++;
            }

            string xml = Serializer.Serialize<List<FTPFileEntry>>(FTPFileEntries);
            Console.Clear();
            Console.WriteLine("Writing file dump.xml");
            File.WriteAllText("dump.xml", xml);
        }

        public static void ReadAllFilesFromDir(string dir)
        {
            string retval = string.Empty;

            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(dir);
            request.Method = WebRequestMethods.Ftp.ListDirectory;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential("anonymous", "dimos@dimos.name");

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            using (Stream responseStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(responseStream))
            {
                retval = reader.ReadToEnd();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(retval);
                Console.WriteLine("Directory List Complete, status {0}", response.StatusDescription);

                reader.Close();
                response.Close();
            }

            var files = retval.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            files.ForEach(
                it =>
                {
                    if (!it.EndsWith(".") && !it.EndsWith(".."))
                    {
                        FTPFileEntries.Add(new FTPFileEntry(dir, it));
                    }
                }
                );
        }

        public static void SetGoogleData(FTPFileEntry entry)
        {
            string url = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false&language=en", entry.NumericLatitude, entry.NumericLongitude);

            using (WebClient wc = new WebClient())
            {
                entry.GeoCodingXMLGoogle = wc.DownloadString(url);
            }
        }

        public static void SetOpenMapsOrgData(FTPFileEntry entry)
        {
            string url = string.Format("http://nominatim.openstreetmap.org/reverse?format=xml&lat={0}&lon={1}&addressdetails=1&accept-language=en", entry.NumericLatitude, entry.NumericLongitude);

            using (WebClient wc = new WebClient())
            {
                entry.GeoCodingXMLOpenMapsOrg = wc.DownloadString(url);
            }
        }

        #endregion

        #region F2

        private static void Selected_F2()
        {
            Console.Clear();
            if (File.Exists("FTPFileEntries.xml"))
            {
                FTPFileEntries = Serializer.Deserialize<List<FTPFileEntry>>(File.ReadAllText("FTPFileEntries.xml"));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("All data has been transferred to memory!");
                ReturnToMenu();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("File does not exist! You either need to get the FTPFileEntries.xml file or download the information (F1))");
                ReturnToMenu();
            }
        }

        #endregion

        #region F3

        private static void Selected_F3()
        {
            bool f3_continue = true;
            while (f3_continue)
            {
                Console.Clear();
                if (FTPFileEntries.Count == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR! No entries exist in memory.");
                    ReturnToMenu();
                }
                else
                {
                    Console.WriteLine("Are you sure? This will take some time. (Y/N)");
                    var selection = Console.ReadKey().Key;
                    if (selection == ConsoleKey.Y)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Clearing existing DB...");
                        DAL.ClearFTPFileEntries();
                        Console.WriteLine("DB Cleared");
                        int counter = 1;
                        foreach (var item in FTPFileEntries)
                        {
                            Console.WriteLine("Inserting entry {0} / {1}", counter, FTPFileEntries.Count);
                            DAL.InsertFTPFileEntry(item);
                            counter++;
                        }
                        Console.WriteLine();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Finished!");
                        ReturnToMenu();
                        f3_continue = false;
                    }
                    else if (selection == ConsoleKey.N)
                    {
                        f3_continue = false;
                    }
                    else
                    {
                        f3_continue = true;
                    }
                }
            }
        }

        #endregion

        #region F4

        private static void Selected_F4()
        {
            Console.Clear();
            if (FTPFileEntries.Count > 0)
            {
                bool f4_continue = true;
                while (f4_continue)
                {
                    Console.Clear();
                    Console.WriteLine("Are you sure? This will take some time. (Y/N)");
                    var selection = Console.ReadKey().Key;
                    if (selection == ConsoleKey.Y)
                    {
                        //do work
                        //keep this commented out, just to avoid doing the whole thing all over again
                        Console.Clear();
                        int counter = 1;
                        foreach (var item in FTPFileEntries)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("{0} / {1}", counter, FTPFileEntries.Count);
                            SetOpenMapsOrgData(item);
                            DAL.UpdateFTPFileEntryOpenMapOrgData(item);
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("Waiting...");
                            Thread.Sleep(1000);
                            counter++;
                        }
                        ReturnToMenu();
                        f4_continue = false;
                    }
                    else if (selection == ConsoleKey.N)
                    {
                        f4_continue = false;
                    }
                    else
                    {
                        f4_continue = true;
                    }
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Memory is empty!");
                ReturnToMenu();
            }
        }

        #endregion

        #region F5

        private static void Selected_F5()
        {
            Console.Clear();
            FTPFileEntries = DAL.GetFTPFileEntries();
            if (FTPFileEntries.Count > 0)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("All data has been transferred to memory!");
                ReturnToMenu();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No data is stored in the database.");
                ReturnToMenu();
            }
        }

        #endregion

        #region F6
        private static void Selected_F6()
        {
            Console.Clear();
            ISSImages = DAL.GetISSImages();
            if (ISSImages.Count > 0)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("All data has been transferred to memory!");
                ReturnToMenu();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("No data is stored in the database.");
                ReturnToMenu();
            }
        }

        #endregion

        #region F7

        private static void Selected_F7()
        {
            bool f7_continue = true;
            while (f7_continue)
            {
                Console.Clear();
                if (ISSImages.Count == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR! No entries exist in memory.");
                    ReturnToMenu();
                }
                else
                {
                    Console.WriteLine("Are you sure? This will take some time. (Y/N)");
                    var selection = Console.ReadKey().Key;
                    if (selection == ConsoleKey.Y)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Clearing existing DB...");
                        DAL.ClearISSImages();
                        Console.WriteLine("DB Cleared");
                        int counter = 1;
                        foreach (var item in ISSImages)
                        {
                            Console.WriteLine("Inserting image {0} / {1}", counter, ISSImages.Count);
                            DAL.InsertISSImage(item);
                            counter++;
                        }
                        Console.WriteLine();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Finished!");
                        ReturnToMenu();
                        f7_continue = false;
                    }
                    else if (selection == ConsoleKey.N)
                    {
                        f7_continue = false;
                    }
                    else
                    {
                        f7_continue = true;
                    }
                }
            }
        }

        #endregion

        #region F8

        public static void Selected_F8()
        {
            bool f8_continue = true;
            while (f8_continue)
            {
                Console.Clear();
                Console.WriteLine("Are you sure? This will take some time. (Y/N)");
                var selection = Console.ReadKey().Key;
                if (selection == ConsoleKey.Y)
                {
                    //C:\Users\Dimos\Desktop\ISS Images

                    string picdir = @"C:\Users\Dimos\Desktop\ISS Images";

                    var files = Directory.EnumerateFiles(picdir).Where(it => it.EndsWith("jpg")).ToList();

                    Console.ForegroundColor = ConsoleColor.White;
                    int counter = 0;
                    foreach (var file in files)
                    {
                        counter++;
                        Console.WriteLine(@"{0} / {1}", counter, files.Count);
                        using (Image img = Bitmap.FromFile(file))
                        {
                            var filename = Path.GetFileName(file);

                            var pic2048 = ScaleImage(img, 2048, 2048);
                            pic2048.Save(string.Format(@"{0}\res2048_{1}", picdir, filename));

                            var pic1024 = ScaleImage(img, 1024, 1024);
                            pic1024.Save(string.Format(@"{0}\res1024_{1}", picdir, filename));

                            var pic512 = ScaleImage(img, 512, 512);
                            pic512.Save(string.Format(@"{0}\res512_{1}", picdir, filename));
                        }
                    }

                    ReturnToMenu();

                    f8_continue = false;
                }
                else if (selection == ConsoleKey.N)
                {
                    f8_continue = false;
                }
                else
                {
                    f8_continue = true;
                }
            }
        }

        private static Bitmap ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            Bitmap bmp = new Bitmap(newImage);

            return bmp;
        }

        #endregion

        #region F12

        private static void Selected_F12()
        {


        }

        #endregion

    }
}