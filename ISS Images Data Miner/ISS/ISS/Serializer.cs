﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ISS
{
    public static class Serializer
    {
        public static string Serialize<T>(T value)
        {
            string retval = string.Empty;
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer xml = new XmlSerializer(typeof(T));
                xml.Serialize(ms, value);
                retval = Encoding.UTF8.GetString(ms.ToArray());
                ms.Close();
                ms.Dispose();
            }
            return retval;
        }

        public static T Deserialize<T>(string contents)
        {
            XmlSerializer xml = new XmlSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(UTF8Encoding.UTF8.GetBytes(contents));
            var result = (T)xml.Deserialize(ms);
            ms.Dispose();
            return result;
        }
    }
}
