﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ISS
{
    //this is the main class that is stored and served later on from the web service
    public class ISSImage
    {
        //parameterless constructor
        public ISSImage()
        { }

        //create the instance from a db record. 
        //we need to let this constructor know if this uses OpenMaps
        public ISSImage(DataRow dr, bool useOpenMapsXML)
        {
            
            this.GoogleAddress = Convert.ToString(dr["Address"]);
            this.GoogleCountry = Convert.ToString(dr["CountryDescription"]);
            this.FTPDirectory = Convert.ToString(dr["FTPDirectory"]);
            this.ImageDateTime = Convert.ToDateTime(dr["FTPFileDateTime"]);
            this.FileName = Convert.ToString(dr["FTPFileName"]);
            this.LexicalLatitude = Convert.ToString(dr["Latitude"]);
            this.LexicalLongitude = Convert.ToString(dr["Longitude"]);
            this.NumericLatitude = Convert.ToDouble(dr["NumericLatitude"]);
            this.NumericLongitude = Convert.ToDouble(dr["NumericLongitude"]);

            //if so, then process the stored OpenMaps XML data accordingly
            if (useOpenMapsXML)
            {
                this.ISSImageId = 0;

                string string_xml = Convert.ToString(dr["OpenMapsOrgData"]);

                var xml = XDocument.Parse(string_xml);

                XElement result = xml.Descendants().Where(it => it.Name.LocalName.Equals("addressparts")).FirstOrDefault();
                var res_road = result.Descendants().Where(it => it.Name.LocalName.Equals("road")).FirstOrDefault();
                OpenMapsRoad = res_road != null ? res_road.Value : string.Empty;
                var res_city = result.Descendants().Where(it => it.Name.LocalName.Equals("city")).FirstOrDefault();
                OpenMapsCity = res_city != null ? res_city.Value : string.Empty;
                var res_state_district = result.Descendants().Where(it => it.Name.LocalName.Equals("state_district")).FirstOrDefault();
                OpenMapsStateDistrict = res_state_district != null ? res_state_district.Value : string.Empty;
                var res_state = result.Descendants().Where(it => it.Name.LocalName.Equals("state")).FirstOrDefault();
                OpenMapsState = res_state != null ? res_state.Value : string.Empty;
                var res_country = result.Descendants().Where(it => it.Name.LocalName.Equals("country")).FirstOrDefault();
                OpenMapsCountry = res_country != null ? res_country.Value : string.Empty;
                var res_country_code = result.Descendants().Where(it => it.Name.LocalName.Equals("country_code")).FirstOrDefault();
                OpenMapsCountryCode = res_country_code != null ? res_country_code.Value : string.Empty;
            }
            else
            {
                this.ISSImageId = Convert.ToInt32(dr["ISSImageId"]);
                OpenMapsRoad = Convert.ToString(dr["OpenMapsRoad"]);
                OpenMapsCity = Convert.ToString(dr["OpenMapsCity"]);
                OpenMapsStateDistrict = Convert.ToString(dr["OpenMapsStateDistrict"]);
                OpenMapsState = Convert.ToString(dr["OpenMapsState"]);
                OpenMapsCountry = Convert.ToString(dr["OpenMapsCountry"]);
                OpenMapsCountryCode = Convert.ToString(dr["OpenMapsCountryCode"]);
            }

        }

        public int ISSImageId { get; set; }

        public string FTPDirectory { get; set; }

        public string FileName { get; set; }

        public string FullFTPPath
        {
            get
            {
                return string.Format("{0}/{1}", FTPDirectory, FileName);
            }
        }

        public DateTime ImageDateTime { get; set; }

        public string LexicalLatitude { get; set; }

        public string LexicalLongitude { get; set; }

        public double NumericLatitude { get; set; }

        public double NumericLongitude { get; set; }

        public string GoogleAddress { get; set; }

        public string GoogleCountry { get; set; }

        public string OpenMapsRoad { get; set; }

        public string OpenMapsCity { get; set; }

        public string OpenMapsStateDistrict { get; set; }

        public string OpenMapsState { get; set; }

        public string OpenMapsCountry { get; set; }

        public string OpenMapsCountryCode { get; set; }

        public override string ToString()
        {
            return GoogleAddress;
        }



    }
}
