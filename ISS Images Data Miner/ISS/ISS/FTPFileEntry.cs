﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ISS
{
    //basic class to handle the first step - bucket entry
    public class FTPFileEntry
    {
        //parameterless constructor
        public FTPFileEntry()
        { }

        //creates the object from a db bucket record 
        public FTPFileEntry(DataRow dr)
        {
            this.FTPFileEntryId = Convert.ToInt32(dr["FTPFileEntryId"]);
            this.Address = Convert.ToString(dr["Address"]);
            this.Country = Convert.ToString(dr["CountryDescription"]);
            this.FTPDirectory = Convert.ToString(dr["FTPDirectory"]);
            this.FTPFileDateTime = Convert.ToDateTime(dr["FTPFileDateTime"]);
            this.FTPFileName = Convert.ToString(dr["FTPFileName"]);
            this.FullFTPPath = Convert.ToString(dr["FullFTPPath"]);
            this.Latitude = Convert.ToString(dr["Latitude"]);
            this.Longitude = Convert.ToString(dr["Longitude"]);
            this.NumericLatitude = Convert.ToDouble(dr["NumericLatitude"]);
            this.NumericLongitude = Convert.ToDouble(dr["NumericLongitude"]);
            this.GeoCodingXMLOpenMapsOrg = Convert.ToString(dr["OpenMapsOrgData"]);

        }

        //manual constructor
        public FTPFileEntry(string dir, string filename)
        {
            FTPDirectory = dir;

            FTPFileName = new string(filename.Skip(3).ToArray());

            //HACK
            //one record contains a 'B'. this is most definitely 'N'
            FTPFileName = FTPFileName.Replace("B", "N");

            FullFTPPath = string.Format("{0}/{1}", FTPDirectory, FTPFileName);

            //process the date data using some linq
            int year = Convert.ToInt32(new string(FTPFileName.Skip(3).Take(4).ToArray()));
            int month = Convert.ToInt32(new string(FTPFileName.Skip(7).Take(2).ToArray()));
            int day = Convert.ToInt32(new string(FTPFileName.Skip(9).Take(2).ToArray()));
            int hour = Convert.ToInt32(new string(FTPFileName.Skip(11).Take(2).ToArray()));
            int minute = Convert.ToInt32(new string(FTPFileName.Skip(13).Take(2).ToArray()));
            int second = Convert.ToInt32(new string(FTPFileName.Skip(15).Take(2).ToArray()));

            //and assign the value
            FTPFileDateTime = new DateTime(year, month, day, hour, minute, second);

            //quick and dirty, just get the filename
            string tmp = FTPFileName.Replace(".zip", string.Empty);
            
            //and extract the subset that contains the coordinates
            string coordinates = new string(tmp.Skip(17).ToArray());

            //split them accordingly
            string lat1 = new string(coordinates.Take(5).ToArray());
            string lon1 = new string(coordinates.Skip(5).ToArray());

            //and create the classic Mercator coordinates, navy style
            Latitude = string.Format("{0}{1}°{2}'",
                new string(lat1.Skip(4).Take(1).ToArray()),
                new string(lat1.Take(2).ToArray()),
                new string(lat1.Skip(2).Take(2).ToArray()));

            Longitude = string.Format("{0}{1}°{2}'",
                new string(lon1.Skip(5).Take(1).ToArray()),
                new string(lon1.Take(3).ToArray()),
                new string(lon1.Skip(3).Take(2).ToArray()));

            string lat2 = string.Format("{0}.{1}",
                new string(lat1.Take(2).ToArray()),
                new string(lat1.Skip(2).Take(2).ToArray()));

            string lon2 = string.Format("{0}.{1}",
                new string(lon1.Take(3).ToArray()),
                new string(lon1.Skip(3).Take(2).ToArray()));

            //handle the horizon here
            if (lat1.Last() == 'N')
            {
                NumericLatitude = double.Parse(lat2);
            }
            else if (lat1.Last() == 'S')
            {
                NumericLatitude = double.Parse(lat2) * -1;
            }
            else
            {
                throw new Exception("Invalid Latitude");
            }

            if (lon1.Last() == 'E')
            {
                NumericLongitude = double.Parse(lon2);
            }
            else if (lon1.Last() == 'W')
            {
                NumericLongitude = double.Parse(lon2) * -1;
            }
            else
            {
                throw new Exception("Invalid Longitude");
            }
        }

        public int FTPFileEntryId { get; set; }

        public string FTPDirectory { get; set; }

        public string FTPFileName { get; set; }

        public string FullFTPPath { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public double NumericLatitude { get; set; }

        public double NumericLongitude { get; set; }

        public DateTime FTPFileDateTime { get; set; }

        public string Address { get; set; }

        public string Country { get; set; }

        public string CountryDescription
        {
            get
            {
                string[] splitter = Address.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                return splitter.Last().TrimStart(new char[] { ' ' });
            }
        }

        //ignoring those - potential license violation. 
        [XmlIgnore]
        private string _GeoCodingXMLGoogle;
        [XmlIgnore]
        public string GeoCodingXMLGoogle
        {
            get
            {
                return _GeoCodingXMLGoogle;
            }
            set
            {
                _GeoCodingXMLGoogle = value;
                //do additional work here if needed
                var xml = XDocument.Parse(_GeoCodingXMLGoogle);
                if (!_GeoCodingXMLGoogle.Contains("ZERO_RESULTS"))
                {
                    XElement result1 = xml.Descendants().Where(it => it.Name.LocalName.Equals("result")).First();
                    Address = result1.Descendants().Where(it => it.Name.LocalName.Equals("formatted_address")).First().Value;
                    XElement result2 = result1.Descendants().Where(it => it.Name.LocalName.Equals("address_component")).Last();
                    //Country = result2.Descendants().Where(it => it.Name.LocalName.Equals("long_name")).First().Value;
                }
                else
                {
                    Address = "N/A";
                    //Country = "N/A";
                }
            }
        }

        [XmlIgnore]
        private string _GeoCodingXMLOpenMapsOrg;
        [XmlIgnore]
        public string GeoCodingXMLOpenMapsOrg
        {
            get
            {
                return _GeoCodingXMLOpenMapsOrg;
            }
            set
            {
                _GeoCodingXMLOpenMapsOrg = value;
                //do additional work here if needed
                if (_GeoCodingXMLOpenMapsOrg != string.Empty)
                {
                    var xml = XDocument.Parse(_GeoCodingXMLOpenMapsOrg);
                    //if (!_GeoCodingXMLGoogle.Contains("ZERO_RESULTS"))
                    //{
                    //    XElement result1 = xml.Descendants().Where(it => it.Name.LocalName.Equals("result")).First();
                    //    Address = result1.Descendants().Where(it => it.Name.LocalName.Equals("formatted_address")).First().Value;
                    //    XElement result2 = result1.Descendants().Where(it => it.Name.LocalName.Equals("address_component")).Last();
                    //    //Country = result2.Descendants().Where(it => it.Name.LocalName.Equals("long_name")).First().Value;
                    //}
                    //else
                    //{
                    //    Address = "N/A";
                    //    //Country = "N/A";
                    //}
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", FullFTPPath, Address);
        }
    }
}
