﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace space_photos.info
{
    public class ISSImage
    {
        public ISSImage()
        { }

        public ISSImage(DataRow dr)
        {

            this.ISSImageId = Convert.ToInt32(dr["ISSImageId"]);
            this.GoogleAddress = Convert.ToString(dr["GoogleAddress"]);
            this.GoogleCountry = Convert.ToString(dr["GoogleCountry"]);
            this.FTPDirectory = Convert.ToString(dr["FTPDirectory"]);
            this.ImageDateTime = Convert.ToDateTime(dr["ImageDateTime"]);
            this.FileName = Convert.ToString(dr["FileName"]);
            this.LexicalLatitude = Convert.ToString(dr["LexicalLatitude"]);
            this.LexicalLongitude = Convert.ToString(dr["LexicalLongitude"]);
            this.NumericLatitude = Convert.ToDouble(dr["NumericLatitude"]);
            this.NumericLongitude = Convert.ToDouble(dr["NumericLongitude"]);
            this.OpenMapsRoad = Convert.ToString(dr["OpenMapsRoad"]);
            this.OpenMapsCity = Convert.ToString(dr["OpenMapsCity"]);
            this.OpenMapsStateDistrict = Convert.ToString(dr["OpenMapsStateDistrict"]);
            this.OpenMapsState = Convert.ToString(dr["OpenMapsState"]);
            this.OpenMapsCountry = Convert.ToString(dr["OpenMapsCountry"]);
            this.OpenMapsCountryCode = Convert.ToString(dr["OpenMapsCountryCode"]);
        }

        public int ISSImageId { get; set; }

        public string FTPDirectory { get; set; }

        public string FileName { get; set; }

        public string FullFTPPath
        {
            get
            {
                return string.Format("{0}/{1}", FTPDirectory, FileName);
            }
        }

        public string ImageFileName 
        { 
            get
            {
                return string.Format("{0}.jpg", Path.GetFileNameWithoutExtension(FileName));
            }
        }

        public DateTime ImageDateTime { get; set; }

        public string LexicalLatitude { get; set; }

        public string LexicalLongitude { get; set; }

        public double NumericLatitude { get; set; }

        public double NumericLongitude { get; set; }

        public string GoogleAddress { get; set; }

        public string GoogleCountry { get; set; }

        public string OpenMapsRoad { get; set; }

        public string OpenMapsCity { get; set; }

        public string OpenMapsStateDistrict { get; set; }

        public string OpenMapsState { get; set; }

        public string OpenMapsCountry { get; set; }

        public string OpenMapsCountryCode { get; set; }

        public override string ToString()
        {
            return GoogleAddress;
        }
    }
}