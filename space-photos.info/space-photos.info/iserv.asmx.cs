﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace space_photos.info
{
    /// <summary>
    /// Summary description for iserv
    /// </summary>
    [WebService(Namespace = "http://space-photos.info/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class iserv : System.Web.Services.WebService
    {
        private void InsertLogEntry(string ServiceCall, string Parameters)
        {
            string IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            DAL.InsertLogEntry(IPAddress, ServiceCall, Parameters, DateTime.Now);
        }


        [WebMethod]
        public List<string> GetCountriesGoogle()
        {
            InsertLogEntry("GetCountriesGoogle", string.Empty);
            return DAL.GetCountriesGoogle();
        }

        [WebMethod]
        public List<string> GetCountriesOpenMaps()
        {
            InsertLogEntry("GetCountriesOpenMaps", string.Empty);
            return DAL.GetCountriesOpenMaps();
        }

        [WebMethod]
        public List<string> GetCountryCodes()
        {
            InsertLogEntry("GetCountryCodes", string.Empty);
            return DAL.GetCountryCodes();
        }

        [WebMethod]
        public List<string> GetCities()
        {
            InsertLogEntry("GetCities", string.Empty);
            return DAL.GetCities();
        }

        [WebMethod]
        public List<ISSImage> GetISSImages()
        {
            InsertLogEntry("GetISSImages", string.Empty);
            return DAL.GetISSImages();
        }

        [WebMethod]
        public List<ISSImage> GetImagesByCountryGoogle(string CountryGoogle)
        {
            InsertLogEntry("GetImagesByCountryGoogle", CountryGoogle);
            return DAL.GetImagesByCountryGoogle(CountryGoogle);
        }

        [WebMethod]
        public List<ISSImage> GetImagesBetweenDates(DateTime FromDate, DateTime ToDate)
        {
            InsertLogEntry("GetImagesBetweenDates", string.Format("{0}|{1}", FromDate.ToString(), ToDate.ToString()));
            return DAL.GetImagesBetweenDates(FromDate, ToDate);
        }

        [WebMethod]
        public List<ISSImage> GetImagesByCountryOpenMaps(string OpenMapsCountry)
        {
            InsertLogEntry("GetImagesByCountryOpenMaps", OpenMapsCountry);
            return DAL.GetImagesByCountryOpenMaps(OpenMapsCountry);
        }

        [WebMethod]
        public List<ISSImage> GetImagesBetweenDatesByCountryGoogle(DateTime FromDate, DateTime ToDate, string CountryGoogle)
        {
            InsertLogEntry("GetImagesBetweenDatesByCountryGoogle", string.Format("{0}|{1}|{2}", FromDate.ToString(), ToDate.ToString(), CountryGoogle));
            return DAL.GetImagesBetweenDatesByCountryGoogle(FromDate, ToDate, CountryGoogle);
        }

        [WebMethod]
        public List<ISSImage> GetImagesBetweenDatesByCountryOpenMaps(DateTime FromDate, DateTime ToDate, string OpenMapsCountry)
        {
            InsertLogEntry("GetImagesBetweenDatesByCountryOpenMaps", string.Format("{0}|{1}|{2}", FromDate.ToString(), ToDate.ToString(), OpenMapsCountry));
            return DAL.GetImagesBetweenDatesByCountryOpenMaps(FromDate, ToDate, OpenMapsCountry);
        }

        [WebMethod]
        public List<ISSImage> GetImagesByCountryCode(string CountryCode)
        {
            InsertLogEntry("GetImagesByCountryCode", CountryCode);
            return DAL.GetImagesByCountryCode(CountryCode);
        }
                
        [WebMethod]
        public List<ISSImage> GetImagesBetweenDatesByCountryCode(DateTime FromDate, DateTime ToDate, string CountryCode)
        {
            InsertLogEntry("GetImagesBetweenDatesByCountryCode", string.Format("{0}|{1}|{2}", FromDate.ToString(), ToDate.ToString(), CountryCode));
            return DAL.GetImagesBetweenDatesByCountryCode(FromDate, ToDate, CountryCode);
        }

        [WebMethod]
        public List<ISSImage> GetImagesByCity(string City)
        {
            InsertLogEntry("GetImagesByCity", City);
            return DAL.GetImagesByCity(City);
        }        

        [WebMethod]
        public List<ISSImage> GetImagesBetweenDatesByCity(DateTime FromDate, DateTime ToDate, string City)
        {
            InsertLogEntry("GetImagesBetweenDatesByCity", string.Format("{0}|{1}|{2}", FromDate.ToString(), ToDate.ToString(), City));
            return DAL.GetImagesBetweenDatesByCity(FromDate, ToDate, City);
        }

    }

}
