﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace space_photos.info
{
    public static class DAL
    {
        
        private static string ConnectionString = "<your mysql connection string>";

        public static void InsertLogEntry(string IPAddress, string ServiceCall, string Parameters, DateTime LogDateTime)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("INSERT INTO log (IPAddress, ServiceCall, Parameters, LogDateTime) VALUES (@paramIPAddress, @paramServiceCall, @paramParameters, @paramLogDateTime)", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@paramIPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@paramServiceCall", ServiceCall);
                cmd.Parameters.AddWithValue("@paramParameters", Parameters);
                cmd.Parameters.AddWithValue("@paramLogDateTime", LogDateTime);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        //GetCountriesGoogle
        public static List<string> GetCountriesGoogle()
        {
            List<string> retlist = new List<string>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT GoogleCountry FROM issimages ORDER BY GoogleCountry ASC", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                conn.Open();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(Convert.ToString(dr["GoogleCountry"]));
                }
                conn.Close();
            }
            return retlist;
        }
                
        //GetCountriesOpenMaps
        public static List<string> GetCountriesOpenMaps()
        {
            List<string> retlist = new List<string>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT OpenMapsCountry FROM issimages ORDER BY OpenMapsCountry ASC", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                conn.Open();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(Convert.ToString(dr["OpenMapsCountry"]));
                }
                conn.Close();
            }
            return retlist;
        }

        //GetCountryCodes
        public static List<string> GetCountryCodes()
        {
            List<string> retlist = new List<string>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT OpenMapsCountryCode AS CountryCode FROM issimages ORDER BY OpenMapsCountryCode ASC", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                conn.Open();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(Convert.ToString(dr["CountryCode"]));
                }
                conn.Close();
            }
            return retlist;
        }

        //GetCities
        public static List<string> GetCities()
        {
            List<string> retlist = new List<string>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT OpenMapsCity AS City FROM issimages ORDER BY OpenMapsCity ASC", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                conn.Open();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(Convert.ToString(dr["City"]));
                }
                conn.Close();
            }
            return retlist;
        }

        //GetISSImages
        public static List<ISSImage> GetISSImages()
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable()) 
            {
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }
        
        //GetImagesByCountryGoogle
        public static List<ISSImage> GetImagesByCountryGoogle(string GoogleCountry)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE GoogleCountry=@paramGoogleCountry", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramGoogleCountry", GoogleCountry);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }
        
        //GetImagesBetweenDates
        public static List<ISSImage> GetImagesBetweenDates(DateTime FromDate, DateTime ToDate)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE ImageDateTime BETWEEN @paramFromDate AND @paramToDate", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramFromDate", FromDate);
                cmd.Parameters.AddWithValue("@paramToDate", ToDate);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }

        //GetImagesByCountryOpenMaps
        public static List<ISSImage> GetImagesByCountryOpenMaps(string OpenMapsCountry)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE OpenMapsCountry=@paramOpenMapsCountry", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramOpenMapsCountry", OpenMapsCountry);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }
        
        //GetImagesBetweenDatesByCountryGoogle
        public static List<ISSImage> GetImagesBetweenDatesByCountryGoogle(DateTime FromDate, DateTime ToDate, string CountryGoogle)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE ImageDateTime BETWEEN @paramFromDate AND @paramToDate AND GoogleCountry=@paramGoogleCountry", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramFromDate", FromDate);
                cmd.Parameters.AddWithValue("@paramToDate", ToDate);
                cmd.Parameters.AddWithValue("@paramGoogleCountry", CountryGoogle);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }
        
        //GetImagesBetweenDatesByCountryOpenMaps
        public static List<ISSImage> GetImagesBetweenDatesByCountryOpenMaps(DateTime FromDate, DateTime ToDate, string OpenMapsGoogle)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE ImageDateTime BETWEEN @paramFromDate AND @paramToDate AND OpenMapsCountry=@paramOpenMapsCountry", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramFromDate", FromDate);
                cmd.Parameters.AddWithValue("@paramToDate", ToDate);
                cmd.Parameters.AddWithValue("@paramOpenMapsCountry", OpenMapsGoogle);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }
        
        //GetImagesByCountryCode
        public static List<ISSImage> GetImagesByCountryCode(string CountryCode)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE OpenMapsCountryCode=@paramOpenMapsCountryCode", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramOpenMapsCountryCode", CountryCode);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }
        
        
        //GetImagesBetweenDatesByCountryCode
        public static List<ISSImage> GetImagesBetweenDatesByCountryCode(DateTime FromDate, DateTime ToDate, string CountryCode)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE ImageDateTime BETWEEN @paramFromDate AND @paramToDate AND OpenMapsCountryCode=@paramOpenMapsCountryCode", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramFromDate", FromDate);
                cmd.Parameters.AddWithValue("@paramToDate", ToDate);
                cmd.Parameters.AddWithValue("@paramOpenMapsCountryCode", CountryCode);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }

        //GetImagesByCity
        public static List<ISSImage> GetImagesByCity(string City)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE OpenMapsCity=@paramOpenMapsCity", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramOpenMapsCity", City);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }        
        
        
        //GetImagesBetweenDatesByCity
        public static List<ISSImage> GetImagesBetweenDatesByCity(DateTime FromDate, DateTime ToDate, string City)
        {
            List<ISSImage> retlist = new List<ISSImage>();
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT ISSImageId, GoogleAddress, GoogleCountry, FTPDirectory, ImageDateTime, FileName, LexicalLatitude, LexicalLongitude, NumericLatitude, NumericLongitude, OpenMapsRoad, OpenMapsCity, OpenMapsStateDistrict, OpenMapsState, OpenMapsCountry, OpenMapsCountryCode FROM issimages WHERE ImageDateTime BETWEEN @paramFromDate AND @paramToDate AND OpenMapsCity=@paramOpenMapsCity", conn))
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            using (DataTable dt = new DataTable())
            {
                cmd.Parameters.AddWithValue("@paramFromDate", FromDate);
                cmd.Parameters.AddWithValue("@paramToDate", ToDate);
                cmd.Parameters.AddWithValue("@paramOpenMapsCity", City);
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    retlist.Add(new ISSImage(dr));
                }
            }
            return retlist;
        }
    }
}