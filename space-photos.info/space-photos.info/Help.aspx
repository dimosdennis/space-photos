﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>space-photos.info - Help</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />
</head>
<body>
    <div id="bg2">
    </div>
    <div id="wrapper1">
        <div id="menu">
            <ul>
                <li><a href="http://www.space-photos.info" class="active">Home</a></li>
                <li><a href="http://www.space-photos.info/iserv.asmx">Data</a></li>
                <li><a href="http://www.space-photos.info/Help.aspx">Help</a></li>
                <li><a href="http://www.space-photos.info/Links.aspx">Links</a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div id="logo">
            <h1>space-photos.info</h1>
            <br />
            <strong>Data Providing Web Service</strong>
        </div>
    </div>

    <div id="header2">
        <div class="mainText3">
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong>space-photos.info</strong>
            <br />
            <br />
            This is an asmx web service.
            <br />
            <br />
            If you are using the .NET platform, you can directly reference the service with Visual Studio and make your requests to the Web Service.
            <br />
            <br />
            Take a look at:
            <br />
            <br />
            <a href="http://msdn.microsoft.com/en-us/library/d9w023sx(v=vs.90).aspx" target="_blank" class="active">How to: Add and Remove Web References</a>
            <br />
            <br />
            <a href="http://msdn.microsoft.com/en-us/library/6h0yh8f9(v=vs.90).aspx" target="_blank" class="active">How to: Call a Web Service</a>
            <br />
            <br />
            However, you can also make HTTP GET requests using any other platform. You will only need to parse the returned XML data after your request.
            <br />
            <br />
            Some example requests:
            <br />
            <br />
            Get a list of all the countries in the system:
            <br />
            <br />
            <code>http://www.space-photos.info/iserv.asmx/GetCountriesOpenMaps</code>
            <br />
            <br />
            Get a list of pictures for a country
            <br />
            <br />
            <code>http://www.space-photos.info/space-photos.info/iserv.asmx/GetImagesByCountryOpenMaps?OpenMapsCountry=Greece</code>
            <br />
            <br />
            Get a list of pictures for a country between a selected date range:
            <br />
            <br />
            <code>http://www.space-photos.info/iserv.asmx/GetImagesBetweenDatesByCountryOpenMaps?FromDate=2012-03-30T12:35:29&ToDate=2014-03-30T12:35:29&OpenMapsCountry=Greece</code>
            <br />
            <br />
            For a complete set of provided web service calls take a look at http://www.space-photos.info/iserv.asmx
            <br />
            <br />
            You are encouraged to drop us a mail for support.
            <br />
            <br />
        </div>
    </div>


    <div id="footer_bot">
        <p>
            Copyright 2014. Created by dimos.xxx
        </p>
        <p>
            <a href="mailto:dimos@dimos.xxx">Contact us</a>
        </p>
    </div>
</body>
</html>
