﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>space-photos.info - Links</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />
</head>
<body>
    <div id="bg2">
    </div>
    <div id="wrapper1">
        <div id="menu">
            <ul>
                <li><a href="http://www.space-photos.info" class="active">Home</a></li>
                <li><a href="http://www.space-photos.info/iserv.asmx">Data</a></li>
                <li><a href="http://www.space-photos.info/Help.aspx">Help</a></li>
                <li><a href="http://www.space-photos.info/Links.aspx">Links</a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div id="logo">
            <h1>space-photos.info</h1>
            <br />
            <strong>Data Providing Web Service</strong>
        </div>

    </div>


    <div id="header2">
        <div class="mainText2">
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong>space-photos.info</strong>
            <br />
            <br />
            Here are some other space photography links:
            <br />
            <br />
            <ul style="padding: 20px">
                <li><a href="https://www.flickr.com/photos/volaremission/" target="_blank">Volare Mission</a><br />
                    Astronaut Luca Parmitano took quite a few beautiful pictures while in orbit in the ISS.
                </li>
                <li><a href="https://www.flickr.com/photos/103865794@N05/sets/72157636126751444/" target="_blank">SlaRos Project 2012</a><br />
                    Stratospheric photography project from Thessaloniki, Greece, first mission.
                </li>
                <li><a href="https://www.flickr.com/photos/103865794@N05/sets/72157636112723816/"target="_blank">SlaRos Project 2013</a><br />
                    Stratospheric photography project from Thessaloniki, Greece, second mission.
                </li>
            </ul>


        </div>
    </div>


    <div id="footer_bot">
        <p>
            Copyright 2014. Created by dimos.xxx
        </p>
        <p>
            <a href="mailto:dimos@dimos.xxx">Contact us</a>
        </p>
    </div>
</body>
</html>
