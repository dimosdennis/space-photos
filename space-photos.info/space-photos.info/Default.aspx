﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>space-photos.info - Home</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="styles.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />
</head>
<body>
    <div id="bg2">
    </div>
    <div id="wrapper1">
        <div id="menu">
            <ul>
                <li><a href="http://www.space-photos.info" class="active">Home</a></li>
                <li><a href="http://www.space-photos.info/iserv.asmx">Data</a></li>
                <li><a href="http://www.space-photos.info/Help.aspx">Help</a></li>
                <li><a href="http://www.space-photos.info/Links.aspx">Links</a></li>
            </ul>
            <div class="clear">

            </div>
        </div>
        <div id="logo">
            <h1>space-photos.info</h1>
            <br />
            <strong>Data Providing Web Service</strong>
        </div>

    </div>


    <div id="header2">
        <div class="mainText2">
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong>space-photos.info</strong>
            <br />
            <br />
            <strong>space-photos.info</strong> currently provides photos taken from the ISS, available on NASA's ISERV server.
            <br />
            <br />
            The data was mined and processed from the ISERV FTP server, adding geolocation and date information.
            <br />
            <br />
            You can use the provided web service to perform searches based on Geolocation metadata and date.
            <br />
            <br />
            We are eager to add more photos in our database from any other source.
            <br />
            <br />
            This data can be used by software developers and other data consumers for any perceivable purpose.
            <br />
            <br />



        </div>
    </div>


    <div id="footer_bot">
        <p>
            Copyright 2014. Created by dimos.xxx
        </p>
        <p>
            <a href="mailto:dimos@dimos.xxx">Contact us</a>
        </p>
    </div>
</body>
</html>
